#!/usr/bin/python3

from btree import Elm
from math import log2, ceil
import sys

def ntoa(s):
	z = ""
	for c in s:
		z += "-+0"[int(c)] + "a"
	return z

def itoa(s, b):
	z = ""
	while s or b:
		z += "-+"[s & 1] + "a"
		s >>= 1
		b -= 1
	return z

def convert0(code):
	l = []
	fix = {}
	for i, x in enumerate(code):
		n = len(l)
		if i == len(code) - 1:
			nx = 0
		else:
			nx = n + 2
		fix[n] = (nx, n + 1)
		l.append(("POP", 0))
		l.append(("PUSH", x, nx))
	if len(l) % 2 != 0:
		l.append(("NOP"))
	for k, v in fix.items():
		n = len(l)
		a, b = v
		l.append(("JUMP", a))
		l.append(("JUMP", b))
		l[k] = ("POP", n)
	l.append(("NOP"))
	return l

def build(n):
	e = Elm("B^")
	if n:
		e.a = build(n - 1)
		e.b = build(n - 1)
	return e

def get_b(bt, i, b):
	f = None
	while b:
		b -= 1
		n = (i >> b) & 1
		f = bt.set_a if n == 0 else bt.set_b
		bt = bt.a if n == 0 else bt.b
	return bt, f

def convert1(initial, code):
	states = convert0(code)
	sbit = ceil(log2(len(states)))
	root = Elm(ntoa(initial) + "-" + "a" * sbit)
	st = [None] * (1 << sbit)
	for i, x in enumerate(states):
		c, *args = x
		e = None
		if c == "PUSH":
			data, nx = args
			e = Elm(ntoa(data) + itoa(nx, sbit) + "0" + "a" * sbit)
		elif c == "POP":
			nx, = args
			e = Elm("A+" + "a" * sbit + "0" + "a" * (sbit - 1) + "^a" + itoa(nx >> 1, sbit - 1) + "0" + "a" * sbit)
		elif c == "JUMP":
			nx, = args
			e = Elm("B" * (sbit * 2 - 1) + itoa(nx, sbit) + "0" + "a" * sbit)
		st[i] = e
	bt = build(sbit - 1)
	root.a = Elm("-", bt)
	for i, x in enumerate(st):
		_, f = get_b(bt, i, sbit)
		f(x)
	root.pad()
	root.reparent()
	#print(root, file=sys.stderr)
	return root.tocode()

if __name__ == "__main__":
	if len(sys.argv) < 3:
		print("usage: %s <initial> <productions...>")
		exit(1)
	print(convert1(sys.argv[1], sys.argv[2:]))
