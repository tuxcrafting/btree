#!/usr/bin/env python3

import getopt
import random
import sys

class Elm:
	def __init__(self, v, a=None, b=None):
		self.v = v
		self.a = a
		self.b = b
		self.p = None
	def height(self):
		p = self.p
		n = 0
		while p:
			p = p.p
			n += 1
		return n
	def max_height(self):
		ah = 0
		bh = 0
		if self.a:
			ah = self.a.max_height()
		if self.b:
			bh = self.b.max_height()
		return 1 + max(ah, bh)
	def pad(self, n=-1):
		if n == -1:
			n = self.max_height() - 1
		if n == 0:
			return
		if self.a is None:
			self.a = Elm(".")
		if self.b is None:
			self.b = Elm(".")
		self.a.pad(n - 1)
		self.b.pad(n - 1)
	def parent(self, p):
		self.p = p
	def set_a(self, a):
		self.a = a
	def set_b(self, b):
		self.b = b
	def get(self, n):
		if n == 0:
			return [self]
		return self.a.get(n - 1) + self.b.get(n - 1)
	def get_s(self):
		s = ""
		for c in self.v[:-1]:
			s += ";" + c
		s += self.v[-1]
		return s
	def tocode(self):
		n = self.max_height()
		l = []
		for i in range(n):
			l += list(map(lambda x: x.get_s(), self.get(i)))
		return "".join(l)
	def reparent(self, p=None):
		self.parent(p)
		if self.a:
			self.a.reparent(self)
		if self.b:
			self.b.reparent(self)
	def repr(self, i):
		if self.a and self.b:
			return "%s%r{\n%s\n%s}{\n%s\n%s}" % (" " * i, self.v, self.a.repr(i + 1), " " * i, self.b.repr(i + 1), " " * i)
		else:
			return "%s%r" % (" " * i, self.v)
	def dot(self, name):
		if self.a and self.b:
			a = "r%d" % random.randint(0, 2**32-1)
			b = "r%d" % random.randint(0, 2**32-1)
			return "%s [label=\"%s\"]\n%s -- %s\n%s -- %s\n%s%s" % (name, self.v, name, a, name, b, self.a.dot(a), self.b.dot(b))
		return "%s [label=\"%s\"]\n" % (name, self.v)
	def __repr__(self):
		return self.repr(0)

class BTree:
	def __init__(self, s, debug=False):
		r = 1
		l = [[]]
		m = 0
		a = ""
		for c in s:
			if m == -1:
				if c == "}":
					m = 0
				continue
			if c == "{":
				m = -1
				continue
			if c <= " ":
				continue
			elif c == ";":
				m += 1
				continue
			if m == 0:
				l[-1].append(a + c)
				a = ""
				r -= 1
			else:
				a += c
				m -= 1
			if r == 0:
				r = 1 << len(l)
				l.append([])
		if not l[-1] and len(l) > 1:
			l.pop()
		if len(l[-1]) != 1 << (len(l) - 1):
			l[-1] += ["."] * ((1 << len(l) - 1) - len(l[-1]))
		q = []
		i = 0
		for x in reversed(l):
			for y in x:
				if i:
					a = q.pop()
					b = q.pop()
					e = Elm(y, a, b)
					a.parent(e)
					b.parent(e)
					q.insert(0, e)
				else:
					q.insert(0, Elm(y))
			i += 1
		self.ip = q.pop()
		self.acc = 0
		self.b = 0
		self.deque = []
		self.halt = False
		self.debug = debug
	def goto(self, e):
		if e is None:
			self.halt = True
		else:
			self.ip = e
	def step(self):
		for c in self.ip.v:
			if c == "0":
				self.acc = 0
			elif c == "-":
				self.acc = -1
			elif c == "+":
				self.acc = 1
			elif c == "^":
				a = self.acc
				self.acc = self.b
				self.b = a
			elif c == "a":
				self.deque.append(self.acc)
			elif c == "A":
				self.b = self.deque.pop(0) if self.deque else 0
			elif c == "b":
				self.deque.insert(0, self.acc)
			elif c == "B":
				self.b = self.deque.pop() if self.deque else 0
			elif c == "&":
				self.acc = min(self.acc, self.b)
			elif c == "|":
				self.acc = max(self.acc, self.b)
			elif c == "=":
				self.acc = 1 if self.acc == self.b else -1
			elif c == "!":
				self.acc = -self.acc
			elif c == "i":
				self.acc = "-0+".index(input()) - 1
			elif c == "o":
				print("0+-"[self.acc])
		if self.debug:
			print("[%s] A=%s B=%s D=%s" % (
				self.ip.v,
				"0+-"[self.acc],
				"0+-"[self.b],
				"".join(map("0+-".__getitem__, self.deque))))
		if self.acc == 0:
			self.goto(self.ip.p)
		elif self.acc == -1:
			self.goto(self.ip.a)
		elif self.acc == 1:
			self.goto(self.ip.b)
	def run(self):
		self.halt = False
		while not self.halt:
			self.step()

def print_usage():
	print("Usage: %s [-dgh] <file>" % sys.argv[0])
	print("\t-d\tDebug mode")
	print("\t-h\tPrint this help message")
	exit(1)

if __name__ == "__main__":
	if len(sys.argv) < 2:
		print_usage()
	try:
		opts, args = getopt.getopt(sys.argv[1:], "dgh")
	except getopt.GetoptError:
		print_usage()
	if len(args) != 1:
		print_usage()
	debug = False
	graph = False
	for x, y in opts:
		if x == "-d":
			debug = True
		elif x == "-g":
			graph = True
		elif x == "-h":
			print_usage()
	if args[0] == "-":
		s = sys.stdin.read()
	else:
		with open(args[0]) as f:
			s = f.read()
	bt = BTree(s, debug)
	if graph:
		print("graph {%s}" % bt.ip.dot("root"))
	else:
		bt.run()
